package utils;

import citySim.Bus;

public class Move {
	public enum Action {
		BUS,
		NORTH,
		SOUTH,
		EAST,
		WEST
	}
	
	private Bus bus;
	private Action action;
	
	public Bus getBus() {
		return bus;
	}

	public void setBus(Bus bus) {
		this.bus = bus;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Move(Action action, Bus bus) {
		this.action = action;
		this.bus = bus;
	}
}