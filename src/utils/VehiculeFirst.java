package utils;

import java.awt.Color;

import citySim.TrafficLight;

public class VehiculeFirst extends Strategy {
	
	public VehiculeFirst(TrafficLight tf) {
		super(tf);
	}

	@Override
	public void execute() {
		if ((tf.getAwaitCars().size() + tf.getAwaitBus().size()) == 0)
			tf.setColor(Color.RED);
		else if (tf.getColor() == Color.RED)
			tf.setColor(Color.GREEN);
	}
	
}
