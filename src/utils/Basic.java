package utils;

import java.awt.Color;

import citySim.TrafficLight;

public class Basic extends Strategy {
	
	private int count = 0;
	
	public Basic(TrafficLight tf) {
		super(tf);
	}

	@Override
	public void execute() {
		if (count >= 10)
			count = 0;
		if (count == 0) {
			if (tf.getColor() == Color.RED)
				tf.setColor(Color.GREEN);
			else
				tf.setColor(Color.RED);
		}
		count++;
	}
	
}
