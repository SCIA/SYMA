package utils;

import javafx.util.Pair;

public class Position {
	
	private int x, y;
	
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public boolean equal(Position pos) {
		return this.x == pos.x && this.y == pos.y;
	}
	
	public Pair<Integer, Integer> distanceFrom(Position pos) {
		int dx = 0;
		int dy = 0;
		if (pos.x > this.x) {
			dx = this.x - pos.x;
			if (pos.y > this.y)
				dy = this.y - pos.y;
			else
				dy = pos.y - this.y;
		} else {
			dx = pos.x - this.x;
		}
		return new Pair<Integer, Integer>(dx, dy);
	}
	
}
