package utils;

import java.awt.Color;

import citySim.TrafficLight;

public class PedestrianFirst extends Strategy {
	
	public PedestrianFirst(TrafficLight tf) {
		super(tf);
	}

	@Override
	public void execute() {
		if (tf.getAwaitPed().size() == 0)
			tf.setColor(Color.GREEN);
		else if (tf.getColor() == Color.GREEN)
			tf.setColor(Color.RED);
	}
	
}
