package utils;

import java.awt.Color;

import citySim.TrafficLight;

public abstract class Strategy {
	protected TrafficLight tf;
	
	public Strategy(TrafficLight tf) {
		this.tf = tf;
	}
	
    public abstract void execute();
}