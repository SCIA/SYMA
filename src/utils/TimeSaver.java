package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import repast.simphony.engine.environment.RunEnvironment;

/**
 *
 * This class is for saving statistics when an agent reached
 * is destination. It will write in asynchronous to avoid slowing the
 * execution.
 */
public class TimeSaver {
	static private BlockingQueue<Time> queue = new LinkedBlockingQueue<>();
	static boolean isRunning = true;
	
	static public class WrittingThread implements Runnable {
		private FileOutputStream fos;
		private BufferedWriter bw;
		
		public WrittingThread() {
			
			try {
				
				fos = new FileOutputStream(new File (RunEnvironment.getInstance().getParameters().getString("outputFileName")));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		 
			bw = new BufferedWriter(new OutputStreamWriter(fos));
		}
		
		
		@Override
		public void run() {
			while (isRunning) {
				try {
					Time time = queue.take();
					bw.write(time.toString());
					bw.flush();
				} catch (InterruptedException e) {
					try {
						bw.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					e.printStackTrace();
				} catch (IOException e) {
					try {
						bw.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					e.printStackTrace();
				}
			}
		}
	}
	
	static public void start() {
		new Thread(new WrittingThread()).start();
	}
	
	static public void push(Time time) {
		queue.add(time);		
	}
}