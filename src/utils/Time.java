package utils;

public class Time {
	private long duration;
	private int distance;
	
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	
	public Time(long duration, int distance) {
		this.duration = duration;
		this.distance = distance;
	}
	
	@Override
	public String toString() {
		return duration + "," + distance + "\r\n";
	}
}
