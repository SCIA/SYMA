package utils;

public enum Direction {
	N,
	E,
	S,
	W,
	NE,
	NW,
	SE,
	SW;
	
	public boolean isEast() {
		return this == Direction.E || this == Direction.NE || this == Direction.SE;
	}
	
	public boolean isWest() {
		return this == Direction.W || this == Direction.NW || this == Direction.SW;
	}

	public boolean isNorth() {
		return this == Direction.N || this == Direction.NW || this == Direction.NE;
	}

	public boolean isSouth() {
		return this == Direction.S || this == Direction.SW || this == Direction.SE;
	}

	static public Direction stringToDirection(String s) {
		if (s.length() == 1 || (s.charAt(1) != 'N' && s.charAt(1) != 'E' && s.charAt(1) != 'S' && s.charAt(1) != 'W'))
		{
			switch(s.charAt(0)) {
			case 'N':
				return Direction.N;
			case 'E':
				return Direction.E;
			case 'S':
				return Direction.S;
			default:
				return Direction.W;
			}
			
		}
		else {
			if (s.charAt(0) == 'N') {
				if (s.charAt(1) == 'E')
					return Direction.NE;
				else
					return Direction.NW;
			}
			else {
				if (s.charAt(1) == 'E')
					return Direction.SE;
				else
					return Direction.SW;
			}
		}
	}
}
