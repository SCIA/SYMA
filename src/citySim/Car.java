package citySim;

import java.awt.Color;
import utils.Position;
import utils.Status;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.ContextUtils;
import utils.AgentType;
import utils.Move;
import repast.simphony.context.Context;

public class Car extends Agent {
	
	private Agent passenger;
	private boolean waitingForPath = false;
	private TrafficLight awaitTL = null;

	public Car(Position pos, Position objective, Grid<Element> grid) {
		super(pos, objective, grid, Color.YELLOW);
		this.type = AgentType.Car;
		this.passenger = null;
	}

	@Override
	@ScheduledMethod(start = 1, interval = 1, priority = 2, shuffle=true)
	public void update() {
		if (path != null)
			waitingForPath = false;
		if (waitingForPath)
			return;
		
		GridPoint pos = this.grid.getLocation(this);
		
		if (passenger == null)
			tick_count = 0;
		
		if (passenger != null && this.path != null && !this.path.isEmpty()) {
			tick_count++;
			
			Context<Agent> context = ContextUtils.getContext(this);
			Move.Action cur = path.getFirst().getAction();
			
			GridPoint newpos = this.grid.getLocation(this);

			switch (cur) {
				case NORTH:
					newpos = new GridPoint(pos.getX(), pos.getY() + 1);
					break;
				case SOUTH:
					newpos = new GridPoint(pos.getX(), pos.getY() - 1);
					break;
				case EAST:
					newpos = new GridPoint(pos.getX() + 1, pos.getY());
					break;
				case WEST:
					newpos = new GridPoint(pos.getX() - 1, pos.getY());
					break;
				default:
					break;
			}

			boolean isPedestrianNextMove = false;
			boolean isParkingCurr = false;
			
			Iterable<Element> objectsAtMyPlace = grid.getObjectsAt(pos.getX(), pos.getY());
			TrafficLight tl = null;
			Road r_curr = null;
			TrafficLight tl_curr= null;
			
			for (Element el: objectsAtMyPlace) {
				if (el instanceof TrafficLight)
				{
					tl = (TrafficLight) el;
					/*if (tl.getColor() == Color.GREEN)
						awaitTL = null;*/
					if (r_curr != null && tl_curr != null)
						break;
				}
				if (el instanceof Road)
				{
					r_curr = (Road) el;
					isParkingCurr = r_curr.isParking();
				}
				if (r_curr != null && tl != null)
					break;
			}
			
			if (r_curr != null) {
				Iterable<Element> objects = grid.getObjectsAt(newpos.getX(), newpos.getY());
				Road r = null;
				Car c = null;
				BusInstance bus = null;
				for (Element el: objects) {
					if (el instanceof Pedestrian)
						isPedestrianNextMove = true;
					if (el instanceof Road)
						r = (Road) el;
					if (el instanceof Car)
						c = (Car) el;
					if (el instanceof BusInstance)
						bus = (BusInstance) el;
					if (r != null && c != null && isPedestrianNextMove)
						break;
				}
				if (r_curr.isParking() && r == null) {
					passenger.addTick_count(tick_count);
					context.add(passenger);
					grid.moveTo(passenger, newpos.getX(), newpos.getY());
					this.path.pop();
					passenger.addTileCount(-this.path.size());
					passenger.setPath(this.path);
					this.passenger = null;
					this.objective = null;
				} else {
					if (r != null) {
						if (c == null && bus == null) {
							boolean can_move = true;
							if (path.size() >= 2) {
								GridPoint in2stp = in2steps(pos, newpos);
								Iterable<Element> twoCaseFurther = grid.getObjectsAt(in2stp.getX(), in2stp.getY());
								Car c1 = null;
								BusInstance b = null;
								for (Element el : twoCaseFurther) {
									if (el instanceof Car) {
										can_move = false;
										c1 = (Car) el;
									}
									if (el instanceof BusInstance) {
										can_move = false;
										b = (BusInstance) el;
									}
									if (can_move == false)
										break;
								}
								if (can_move == false) {
									if (c1 != null)
										this.awaitTL = c1.getTL();
									if (b != null)
										this.awaitTL = b.getTL();
								} else {
									if (c1 == null && b == null) {
										can_move = true;
										if (awaitTL != null) {
											awaitTL.unregister(this);
										}
										awaitTL = null;
										
									}
								}
							}
							can_move = false;
							if (tl != null) {
								if (tl.getColor() == Color.GREEN) {
									if (this.awaitTL != null) {
										tl.unregister(this);
										this.awaitTL = null;;
									}
									if (r.isCrossable() && !isPedestrianNextMove) {
										can_move = true;
									}
								} else {
									tl.register(this);
									this.awaitTL = tl;
								}
							} else {
								can_move = true;
							}
							
							if (awaitTL != null)
								return;
							
							if (can_move) {
								grid.moveTo(this, newpos.getX(), newpos.getY());
								this.path.pop();
							}
						} else if (r.isParking()) {
							ArrayList<Position> blocked = new ArrayList<>();
							blocked.add(new Position(newpos.getX(), newpos.getY()));
							if (path != null)
								passenger.addTileCount(-path.size());
							path = null;
							BlackBoard.getPath(this, blocked);
							waitingForPath = true;
							return;
						} else {
							if (c != null)
								this.awaitTL = c.awaitTL;
							else if (bus != null)
								this.awaitTL = bus.getTL();
							if (this.awaitTL != null)
								return;
						}
					}
				}
			}
		} else if (this.passenger != null && (this.path == null || this.path.isEmpty())) {
			path = null;
			BlackBoard.getPath(this, null);
			waitingForPath = true;
			return;
		}
	}
	
	public boolean hasPassenger() {
		return passenger != null;
	}
	
	private GridPoint in2steps(GridPoint pos, GridPoint newpos) {
		int width = this.grid.getDimensions().getWidth();
		int height = this.grid.getDimensions().getHeight();
		int diffx = newpos.getX() - pos.getX();
		int diffy = newpos.getY() - pos.getY();
		if (newpos.getX() + diffx >= width || newpos.getX() + diffx < 0 ||  newpos.getY() + diffy < 0 ||  newpos.getY() + diffy >= height)
			return newpos;
		return new GridPoint(newpos.getX() + diffx, newpos.getY() + diffy);
	}
	
	public void addPassenger(Agent pedestrian) {
		Context<Agent> context = ContextUtils.getContext(this);
		this.passenger = pedestrian;
		this.path = passenger.path;
		this.objective = pedestrian.getObjective();
		context.remove(pedestrian);
	}
	
	@Override
	public void setPath(LinkedList<Move> path) {
		super.setPath(path);
		passenger.addTileCount(tiles_count);
	}

	public TrafficLight getTL() {
		return this.awaitTL;
	}
	
}