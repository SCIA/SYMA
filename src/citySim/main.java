package citySim;

import repast.simphony.context.Context;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.space.grid.BouncyBorders;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.SimpleGridAdder;
import utils.Direction;
import utils.Position;
import utils.TimeSaver;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import citySim.Element;

public class main implements ContextBuilder<Element>{
	
	@Override
	public Context<Element> build(Context<Element> context) {
		context.setId("citySim");

		TreeMap<Integer, Bus> buses = new TreeMap<>();
		Grid<Element> grid = parseMap(context, buses);
		parseAgent(context, grid, buses);
		return context;
	}
	private void parseAgent(Context<Element> context, Grid<Element> grid, TreeMap<Integer, Bus> buses)
	{
		ArrayList<String> file = new ArrayList<>(); 
		String agentFileName = RunEnvironment.getInstance().getParameters().getString("agentFileName");
		try {
			file = (ArrayList)Files.readAllLines(Paths.get("src/resources/agents/" + agentFileName));
		} catch (IOException e) {
			System.err.println("citySim: Couldn't find the agent list");
		}
		int width = grid.getDimensions().getWidth();
		int height = grid.getDimensions().getHeight();
		for(String line : file) {
			String[] splitLine = line.split(",");
			switch(splitLine[0]) {
				case "P":
					Position pos = new Position(Integer.parseInt(splitLine[1]), Integer.parseInt(splitLine[2]));
					if (pos.getX() < 0 || pos.getY() < 0 || pos.getX() >= width || pos.getY() >= height)
						continue;
					Position obj = null;
					Position carPos = null;
					if (splitLine.length >= 6) {
						obj = new Position(Integer.parseInt(splitLine[3]), Integer.parseInt(splitLine[4]));
						carPos = new Position(Integer.parseInt(splitLine[5]), Integer.parseInt(splitLine[6]));
					}
					else
						carPos = new Position(Integer.parseInt(splitLine[3]), Integer.parseInt(splitLine[4]));
					Pedestrian p = null;
					if (carPos == null || carPos.getX() < 0 || carPos.getY() < 0 || carPos.getX() >= width || carPos.getY() >= height)
						p = new Pedestrian(pos, obj, grid, null);
					else {
						Iterable<Element> elements = grid.getObjectsAt(carPos.getX(), carPos.getY());
						Car c = null;
						for (Element e : elements) {
							if (e instanceof Car) {
								c = (Car)e;
								break;
							}
						}
						p = new Pedestrian(pos, obj, grid, c);
					}
					context.add(p);
					grid.moveTo(p, pos.getX(), pos.getY());
					break;
				case "C":
					Position pos2 = new Position(Integer.parseInt(splitLine[1]), Integer.parseInt(splitLine[2]));
					if (pos2.getX() < 0 || pos2.getY() < 0 || pos2.getX() >= width || pos2.getY() >= height)
						continue;
					Car c = new Car(pos2, null, grid);
					context.add(c);
					grid.moveTo(c, pos2.getX(), pos2.getY());
					break;
				case "BS":
					Position posbs = new Position(Integer.parseInt(splitLine[1]), Integer.parseInt(splitLine[2]));
					if (posbs.getX() < 0 || posbs.getY() < 0 || posbs.getX() >= width || posbs.getY() >= height)
						continue;
					Position posbdespawn = new Position(Integer.parseInt(splitLine[3]), Integer.parseInt(splitLine[4]));
					if (posbdespawn.getX() < 0 || posbdespawn.getY() < 0 || posbdespawn.getX() >= width || posbdespawn.getY() >= height)
						continue;
					context.add(new BusSpawner(context, grid, buses, posbs, posbdespawn));
					break;
				default:
					break;
			}
		}
		
	}
	private Grid<Element> parseMap(Context<Element> context, TreeMap<Integer, Bus> buses)
	{
		ArrayList<String> file = new ArrayList<>(); 
		String mapFileName = RunEnvironment.getInstance().getParameters().getString("mapFileName");
		try {
			file = (ArrayList)Files.readAllLines(Paths.get("src/resources/maps/" + mapFileName));
		} catch (IOException e) {
			System.err.println("citySim: Couldn't find the map");
		}
		
		ArrayList<String[]> split_csv = new ArrayList<>();
		for(String line : file)
			split_csv.add(line.split(","));
		
		Integer width = Integer.parseInt(split_csv.get(0)[0]);
		Integer height = Integer.parseInt(split_csv.get(0)[1]);
		split_csv.remove(0);

		GridFactory gridFactory = GridFactoryFinder.createGridFactory(null);
		Grid<Element> grid = gridFactory.createGrid("grid", context, new GridBuilderParameters<Element>(
				new BouncyBorders(), new SimpleGridAdder<Element>(), true, width, height));
		
		HashMap<Integer, TreeMap<Integer, BusStop>> busStops = new HashMap<>();
		
		for(int j = 0; j < height; j++) {
			for(int i = 0; i < width; i++) {
				String cur = split_csv.get(j)[i];
				Tile t;
				Car car = null;
				switch(cur.charAt(0)) {
					case 'P':
						t = new Road(Direction.stringToDirection(cur.substring(1)), true);
						break;
					case 'R':
						t = new Road(Direction.stringToDirection(cur.substring(1)), false);
						if (cur.contains("C"))
						{
							if (cur.endsWith("C"))
								((Road)t).makeCrossable();
							else {
								String[] coord = cur.substring(cur.indexOf('C') + 2, cur.indexOf(']')).split(";");
								((Road)t).makeCrossableWithTL(context, grid,
									Integer.parseInt(coord[0]), Integer.parseInt(coord[1]), Integer.parseInt(coord[2]));
							}
						}
						break;
					case 'S':
						t = new Sidewalk();
						if (cur.contains("BS"))
						{
							String[] busInfos = cur.substring(cur.indexOf('[') + 1, cur.indexOf(']')).split(";");
							for (String bus : busInfos) {
								String[] infos = bus.split("\\|");
								TreeMap<Integer, BusStop> cur_busStops;
								Integer busID = Integer.parseInt(infos[0]);
								if (busStops.containsKey(busID))
									cur_busStops = busStops.get(busID);
								else
									cur_busStops = new TreeMap<>();
								BusStop cur_bs = new BusStop(5, new Position(i, height - 1 - j), grid);
								cur_busStops.put(Integer.parseInt(infos[1]), cur_bs);
								busStops.put(busID, cur_busStops);
								context.add(cur_bs);
								grid.moveTo(cur_bs, i, height - 1 - j);
							}
						}
						break;
					default:
						t = new None();
						break;
				}
				context.add(t);
				grid.moveTo(t, i, height - 1 - j); //The graphic result works like Cartesian coordinates(taking its origin in the bottom left corner)
			}
		}

		new Thread(new BlackBoard(grid)).start();
		TimeSaver.start();
		
		for (Integer busID : busStops.keySet()) {
			Bus b = new Bus(busID, new TreeMap<>(), 25);
			TreeMap<Integer, BusStop> stops = busStops.get(busID);
			for (Integer entry : stops.keySet()) {
				BusStop busStop = stops.get(entry);
				busStop.addBus(b);
				b.addBusStop(busStop);
			}
			buses.put(busID, b);
		}
		return grid;
	}
	
}