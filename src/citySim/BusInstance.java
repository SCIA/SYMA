package citySim;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.TreeMap;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.ContextUtils;
import utils.Move;
import utils.Position;
import utils.Status;

public class BusInstance extends Agent {
	private int nbPedMax;
	private int nbPedCur;
	private Bus busType;
	private boolean shouldStop = true;
	private ArrayList<Pedestrian> passengers = new ArrayList<>();
	private TrafficLight awaitTL = null;
	
	public BusInstance(Position pos, Position objective, Grid<Element> grid, Bus busType) {
		super(pos, objective, grid, Color.MAGENTA);
		this.busType = busType;
		this.nbPedMax = 25;
		this.nbPedCur = 0;
		this.path = new Dijkstra(BlackBoard.getGrid(), Status.BUS, null, null).getShortestPath(pos, busType.getBusStops().firstKey().getClosestRoadPosition());
		this.path.addAll(busType.getLinePath());
	}

	public int getNbPedMax() {
		return nbPedMax;
	}

	public void setNbPedMax(int nbPedMax) {
		this.nbPedMax = nbPedMax;
	}
	
	private BusStop isAtBusStop(TreeMap<BusStop, Integer> busStops, GridPoint pos) {
		for (BusStop bs : busStops.keySet()) {
			GridPoint bsPos = this.grid.getLocation(bs);
			if (Math.abs(bsPos.getX() - pos.getX()) + Math.abs(bsPos.getY() - pos.getY()) <= 1)
				return bs;
		}
		return null;
	}
	
	private GridPoint in2steps(GridPoint pos, GridPoint newpos) {
		int width = this.grid.getDimensions().getWidth();
		int height = this.grid.getDimensions().getHeight();
		int diffx = newpos.getX() - pos.getX();
		int diffy = newpos.getY() - pos.getY();
		if (newpos.getX() + diffx >= width || newpos.getX() + diffx < 0 ||  newpos.getY() + diffy < 0 ||  newpos.getY() + diffy >= height)
			return newpos;
		return new GridPoint(newpos.getX() + diffx, newpos.getY() + diffy);
	}
	
	int getDistFromPrev(BusStop busStop) {
		Iterator<Entry<BusStop, Integer>> iterator = busType.getBusStops().entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<BusStop, Integer> prev = iterator.next();
			if (iterator.hasNext()) {
				Entry<BusStop, Integer> cur = iterator.next();
				if (cur.getKey() == busStop)
					return prev.getValue();
			}
		}
		return 0;
	}
	
	@Override
	@ScheduledMethod(start = 1, interval = 1, priority = 2, shuffle=true)
	public void update() {
		tick_count++;
		GridPoint pos = this.grid.getLocation(this);
		BusStop currentBusStop =  this.isAtBusStop(this.busType.getBusStops(), pos);
		if (currentBusStop != null && (currentBusStop.getPedestrians().size() > 0 || passengers.size() > 0) && shouldStop)
		{
			Iterator<Pedestrian> iterator = passengers.iterator();
			while(iterator.hasNext()) {
				Pedestrian p = iterator.next();
				if (p.leaveBus(pos, ContextUtils.getContext(this), busType.getBusID(), currentBusStop, getDistFromPrev(currentBusStop))) {
					p.addTick_count(tick_count);
					iterator.remove();
				}
			}
			Context<Element> context = ContextUtils.getContext(this);
			iterator = currentBusStop.getPedestrians().iterator();
			while(iterator.hasNext())
			{
				Pedestrian p = iterator.next();
				if (p.joinBus(this.busType.getBusID())) {
					p.addTick_count(- tick_count);
					passengers.add(p);
					iterator.remove();
				}
			}
			this.shouldStop = false;
			return;
		}
		
		if (path == null || path.isEmpty()) {
			if (pos.getX() == objective.getX() && pos.getY() == objective.getY()) {
				Context<Element> context = ContextUtils.getContext(this);
				context.remove(this);
				return;
			}
			else {
				LinkedList<Move> pathToAdd = new Dijkstra(BlackBoard.getGrid(), Status.BUS,
						null, null).getShortestPath(new Position(pos.getX(), pos.getY()), objective);
				this.path.addAll(pathToAdd);
			}
				
		}
		
		Move.Action cur = this.path.getFirst().getAction();
		
		GridPoint newpos = this.grid.getLocation(this);
		switch (cur) { 
			case NORTH:
				newpos = new GridPoint(pos.getX(), pos.getY() + 1);
				break;
			case SOUTH:
				newpos = new GridPoint(pos.getX(), pos.getY() - 1);
				break;
			case EAST:
				newpos = new GridPoint(pos.getX() + 1, pos.getY());
				break;
			case WEST:
				newpos = new GridPoint(pos.getX() - 1, pos.getY());
				break;
			default:
				break;
		}
		
		Iterable<Element> objectsAtMyPlace = grid.getObjectsAt(pos.getX(), pos.getY());
		TrafficLight tl = null;
		
		for (Element el: objectsAtMyPlace) {
			if (el instanceof TrafficLight)
			{
				tl = (TrafficLight) el;
				if (tl.getColor() == Color.RED) {
					awaitTL = tl;
					tl.register(this);
					return;
				} else {
					awaitTL = null;
					tl.unregister(this);
				}
			}
		}
		
		Iterable<Element> objectsAtNextPlace = grid.getObjectsAt(newpos.getX(), newpos.getY());
		Car c = null;
		BusInstance b = null;
		boolean can_move = true;
		for (Element el: objectsAtNextPlace) {
			if (el instanceof Agent)
				can_move = false;
		}

		GridPoint stepsPos = in2steps(pos, newpos);
		
		Iterable<Element> objects = grid.getObjectsAt(stepsPos.getX(), stepsPos.getY());
		for (Element el: objects) {
			if (el instanceof Car)
				c = (Car) el;
			if (el instanceof BusInstance)
				b = (BusInstance) el;
			if (el instanceof Agent)
				can_move = false;
		}
		if (c != null)
			this.awaitTL = c.getTL();
		else if (b != null)
			this.awaitTL = b.getTL();
		if (awaitTL != null) {
			awaitTL.register(this);
			can_move = false;
		}
		if (c == null && b == null) {
			can_move = true;
			if (awaitTL != null) {
				awaitTL.unregister(this);
			}
			awaitTL = null;
		}
		if (can_move) {
			shouldStop = true;
			grid.moveTo(this, newpos.getX(), newpos.getY());
			this.path.pop();
		}
	}
	
	public TrafficLight getTL() {
		return this.awaitTL;
	}

}
