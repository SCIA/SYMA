package citySim;

import java.awt.Color;

import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.ContextUtils;
import utils.Direction;

public class Road extends Tile{
	private Direction direction;
	private boolean crossable;
	private boolean parking = false;
	private TrafficLight tl = null;
	
	public Road(Direction direction, boolean parking) { //Simple Road
		super(parking ? Color.blue: Color.LIGHT_GRAY);
		this.setDirection(direction);
		this.crossable = false;
		this.parking = parking;
	}

	public void makeCrossableWithTL(Context<Element> context,Grid<Element> grid, int starting_tick, int x, int y) { // Crossable Road
		this.color = Color.WHITE;
		this.crossable = true;
		Iterable<Element> objects = grid.getObjectsAt(x, y);
		for (Element el : objects)
		{
			if (el instanceof TrafficLight)
				this.tl = (TrafficLight) el;
		}
		if (tl == null) {
			this.tl = new TrafficLight(starting_tick == 0? Color.RED: Color.GREEN);
			context.add(this.tl);
			grid.moveTo(this.tl, x, y);
		}
	}
	
	public void makeCrossable() { // Crossable Road
		this.color = Color.WHITE;
		this.crossable = true;
	}
	
	public boolean isCrossable() {
		return crossable;
	}
	
	public boolean isParking() {
		return this.parking;
	}

	public void setCrossable(boolean crossable) {
		this.crossable = crossable;
	}
	
	public void setParking(boolean parking) {
		this.parking = parking;
	}

	public TrafficLight getTl() {
		return this.tl;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
		}
}
