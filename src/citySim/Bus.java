package citySim;

import java.util.Map.Entry;

import utils.Move;
import utils.Status;

import java.util.LinkedList;
import java.util.TreeMap;

public class Bus {
	private int busID;
	private int timeToSpawn = 0;
	private TreeMap<BusStop, Integer> busStops;
	private LinkedList<Move> linePath = new LinkedList<>();
	
	public Bus(int busId,  TreeMap<BusStop, Integer> busStops, int timeToSpawn) {
		this.busStops = busStops;
		this.busID = busId;
		this.timeToSpawn = timeToSpawn;
	}
	
	public int getTimeToSpawn() {
		return timeToSpawn;
	}
	
	public TreeMap<BusStop, Integer> getBusStops() {
		return busStops;
	}

	public void setBusStops(TreeMap<BusStop, Integer> busStops) {
		this.busStops = busStops;
	}
	
	public void addBusStop(BusStop bs) {
		if (this.busStops.isEmpty()) {
			this.busStops.put(bs, 0);
			return;
		}
		BusStop last = this.busStops.lastKey();
		LinkedList<Move> pathToAdd = new Dijkstra(BlackBoard.getGrid(), Status.BUS, null, null).getShortestPath(last.getClosestRoadPosition(), bs.getClosestRoadPosition());
		this.linePath.addAll(pathToAdd);
		Integer distPrev = pathToAdd.size();
		this.busStops.put(last, distPrev);
		this.busStops.put(bs, 0);
	}

	public int getBusID() {
		return busID;
	}

	public BusStop getBusStop(Integer id) {
		 for (Entry<BusStop, Integer> entry : this.busStops.entrySet())
		 {
			 if (entry.getValue() == id)
				 return entry.getKey();
		 }
		 return null;
	}
	
	public void setBusID(int busID) {
		this.busID = busID;
	}

	public LinkedList<Move> getLinePath() {
		return linePath;
	}

	public void setLinePath(LinkedList<Move> linePath) {
		this.linePath = linePath;
	}
}