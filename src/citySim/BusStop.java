package citySim;

import java.awt.Color;
import java.util.ArrayList;

import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.ContextUtils;
import utils.Position;

public class BusStop extends Tile implements Comparable<BusStop> {
	private Position position;
	private ArrayList<Pedestrian> pedestrians = new ArrayList<>();
	private ArrayList<Bus> buses = new ArrayList<>();
	private int capacity = 0;
	private Grid<Element> grid;

	public BusStop(int capacity, Position position, Grid<Element> grid) {
		super(Color.MAGENTA);
		this.capacity = capacity;
		this.position = position;
		this.grid = grid;
	}

	public Position getClosestRoadPosition()
	{
		
		if (position.getX() - 1 >= 0) {
			Iterable<Element> objects1 = grid.getObjectsAt(position.getX() - 1, position.getY());
			for (Element e: objects1)
			{
				if (e instanceof Road)
				{
					GridPoint gp = grid.getLocation(e);
					return new Position(gp.getX(), gp.getY());
				}
			}
		}
		if (position.getY() - 1 >= 0) {
			Iterable<Element> objects3 = grid.getObjectsAt(position.getX(), position.getY() - 1);
			for (Element e: objects3)
			{
				if (e instanceof Road)
				{
					GridPoint gp = grid.getLocation(e);
					return new Position(gp.getX(), gp.getY());
				}
			}
		}
		
		int width = grid.getDimensions().getWidth();
		if (position.getX() + 1 < width) {
			Iterable<Element> objects = grid.getObjectsAt(position.getX() + 1, position.getY());
			for (Element e: objects)
			{
				if (e instanceof Road)
				{
					GridPoint gp = grid.getLocation(e);
					return new Position(gp.getX(), gp.getY());
				}
			}
		}
		
		int height = grid.getDimensions().getHeight();
		if (position.getY() + 1 < height) {
			Iterable<Element> objects2 = grid.getObjectsAt(position.getX(), position.getY() + 1);
			for (Element e: objects2)
			{
				if (e instanceof Road)
				{
					GridPoint gp = grid.getLocation(e);
					return new Position(gp.getX(), gp.getY());
				}
			}
		}
		
		return null;
	}
	
	public Position getPosition() {
		return position;
	}

	public ArrayList<Bus> getBuses() {
		return buses;
	}
	
	@Override
	public int compareTo(BusStop other) {
		return this == other ? 0 : 1;
	}

	public int getCapacity() {
		return capacity;
	}
	
	public void addPedestrian(Pedestrian p) {
		pedestrians.add(p);
	}
	
	public void addBus(Bus bus) {
		buses.add(bus);
	}

	public ArrayList<Pedestrian> getPedestrians() {
		return pedestrians;
	}

	public void setPedestrians(ArrayList<Pedestrian> pedestrians) {
		this.pedestrians = pedestrians;
	}
}