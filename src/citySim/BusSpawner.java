package citySim;

import java.awt.Color;
import java.util.ArrayList;
import java.util.TreeMap;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.ContextUtils;
import utils.Position;

public class BusSpawner extends Element {
	private boolean spawned = false; 
	private Grid<Element> grid;
	private Bus[] buses;
	private int[] timeLeft;
	private Position spawn;
	private Position despawn;
	private Context<Element> context;
	
	public BusSpawner(Context<Element> context, Grid<Element> grid, TreeMap<Integer, Bus> bus, Position spawn, Position despawn) {
		super(Color.BLACK);
		this.grid = grid;
		this.buses = new Bus[bus.values().size()];
		int i = 0;
		for (Object object: bus.values()) {
			buses[i++] = (Bus) object;
		}
		this.spawn = spawn;
		this.despawn = despawn;
		timeLeft = new int[buses.length];
		for (i = 0; i < buses.length; i++)
			timeLeft[i] = buses[i].getTimeToSpawn();
		this.context = context;
	}
	
	@ScheduledMethod(start = 1, interval = 1, priority = 2, shuffle=true)
	public void update() {
		if (spawned) {
			for (int i = 0; i < timeLeft.length; i++)
				if (timeLeft[i] > 0)
					timeLeft[i]--;
			spawned = false;
			return;
		}
			
		for (int i = 0; i < timeLeft.length; i++) {
			if (timeLeft[i] == 0 && !spawned) {
				BusInstance busInstance = new BusInstance(spawn, despawn, grid, buses[i]);
				context.add(busInstance);
				grid.moveTo(busInstance, spawn.getX(), spawn.getY());
				timeLeft[i] = buses[i].getTimeToSpawn();
				spawned = true;
			} else if (timeLeft[i] > 0)
				timeLeft[i]--;
		}
	}
}
