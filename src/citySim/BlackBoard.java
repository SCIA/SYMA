package citySim;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import repast.simphony.space.grid.Grid;
import utils.Position;
import utils.Status;

public class BlackBoard implements Runnable {
	
	public BlackBoard(Grid<Element> g) {
		
		linkedBlockingQueue = new LinkedBlockingQueue<>();
		setGrid(g);
	}
	
	private static LinkedBlockingQueue<PathGetter> linkedBlockingQueue;
	private static ArrayList<Element>[][] grid;
	public static ArrayList<Element>[][] getGrid() {
		return grid;
	}
	
	public void setGrid(Grid<Element> g) {
		grid = new ArrayList[g.getDimensions().getHeight()][g.getDimensions().getWidth()];
		for (int i = 0; i < g.getDimensions().getHeight(); i++) {
			for (int j = 0; j < g.getDimensions().getWidth(); j++) {
				ArrayList<Element> objects = new ArrayList<>();
				for (Object object: g.getObjectsAt(i, j))
					if (object instanceof Road || object instanceof Sidewalk || object instanceof BusStop)
						objects.add((Element) object);
				grid[i][j] = objects;
			}
		}
	}
	
	private static class PathGetter implements Runnable {
		private Agent agent;
		private ArrayList<Element>[][] grid;
		private ArrayList<Position> blocked;
		
		public PathGetter(Agent agent, ArrayList<Element>[][] grid, ArrayList<Position> blocked) {
			this.agent = agent;
			this.grid = grid;
			this.blocked = blocked;
		}
		
		private boolean isValidOp(boolean ped, int x, int y) {
			for (Object object: grid[x][y]) {
				if (object instanceof Road)
					return !ped;
				if (object instanceof Sidewalk)
					return ped;
			}
			return false;
		}

		@Override
		public void run() {
			boolean ped = agent instanceof Pedestrian;
			if (agent.getObjective() == null) {
				Random random = new Random();
				int x, y;
				do {
					x = random.nextInt(grid.length);
					y = random.nextInt(grid[0].length);
				} while(!isValidOp(ped, x, y));
				agent.setObjective(new Position(x, y));
			}
			agent.setPath(new Dijkstra(grid, ped ? Status.PED : (agent instanceof Car ? Status.CAR : Status.BUS) , blocked,
					ped ? ((Pedestrian)agent).getCar() : null).getShortestPath(agent.getPosition(), agent.getObjective()));
		}
	}
	
	public static void getPath(Agent agent, ArrayList<Position> blocked) {
		linkedBlockingQueue.add(new PathGetter(agent, grid, blocked));
	}

	@Override
	public void run() {
		while(true) {
			try {
				linkedBlockingQueue.take().run();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}