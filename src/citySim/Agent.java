package citySim;

import java.util.LinkedList;
import java.awt.Color;
import repast.simphony.space.grid.Grid;
import utils.Position;
import utils.AgentType;
import utils.Move;

public abstract class Agent extends Element {
	protected Position pos;
	protected int tiles_count;
	protected int tick_count;
	protected AgentType type;
	protected Position objective;
	protected Grid<Element> grid;
	protected LinkedList<Move> path;
	
	public Agent(Position pos, Position objective, Grid<Element> grid, Color color) {
		super(color);
		this.tiles_count = 0;
		this.objective = objective;
		this.grid = grid;
		this.pos = pos;
	}
	
	public abstract void update();
	
	public AgentType getType() {
		return type;
	}
	
	public Position getObjective() {
		return objective;
	}

	public Position getPosition() {
		return new Position(grid.getLocation(this).getX(),grid.getLocation(this).getY());
	}

	public void setObjective(Position obj) {
		this.objective = obj;
	}
	
	public void setPosition(Position pos) {
		this.pos = pos;
	}
	
	public int getTileCount() {
		return tiles_count;
	}
	
	public void setTileCount(int nb) {
		tiles_count = nb;
	}
	
	public void addTileCount(int nb) {
		tiles_count += nb;
	}
	
	public void setPath(LinkedList<Move> path) {
		if (path != null)
			tiles_count += path.size();
		this.path = path;
	}

	public int getTick_count() {
		return tick_count;
	}

	public void setTick_count(int tick_count) {
		this.tick_count = tick_count;
	}
	
	public void addTick_count(int tick_count) {
		this.tick_count += tick_count;
	}
}