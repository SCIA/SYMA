package citySim;

import java.awt.Color;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.ContextUtils;
import utils.AgentType;
import utils.Move;
import utils.Position;
import utils.Time;
import utils.TimeSaver;

public class Pedestrian extends Agent {
	private boolean waitingForPath = false;
	private Car car = null;
	private boolean atBusStop = false;
	private TrafficLight tl = null;
	private boolean randomPath;
	
	public Pedestrian(Position pos, Position objective, Grid<Element> grid, Car car) {
		super(pos, objective, grid, Color.CYAN);
		this.type = AgentType.Pedestrian;
		this.setCar(car);
		this.randomPath = objective == null;
	}
	
	public boolean isAtBusStop() {
		return this.atBusStop;
	}
	
	public boolean joinBus(Integer busId) {
		Move cur = this.path.getFirst();
		if (cur != null && cur.getBus().getBusID() == busId) {
			this.path.pop();
			this.atBusStop = false;
			Context<Element> context = ContextUtils.getContext(this);
			context.remove(this);
			return true;
		}
		return false;
	}
	
	public boolean leaveBus(GridPoint busPos, Context<Element> context, int busId, BusStop busStop, int dist) {
		addTileCount(dist);
		if (this.path.size() > 0) {
			Move cur = this.path.getFirst();
			if (cur.getAction() == Move.Action.BUS && cur.getBus().getBusID() == busId) {
				this.path.pop();
				return false;
			}
		}
		context.add(this);
		grid.moveTo(this, busStop.getPosition().getX(), busStop.getPosition().getY());
		return true;
	}
	
	@Override
	@ScheduledMethod(start = 1, interval = 2, priority = 2, shuffle=true)
	public void update() {
		if (path != null)
			waitingForPath = false;
		if (waitingForPath)
			return;
		
		if (path == null || path.isEmpty()) {
			if (tiles_count != 0)
				TimeSaver.push(new Time(tick_count, tiles_count));
			tick_count = 0;
			tiles_count = 0;
			if (this.randomPath)
				objective = null;
			path = null;
			BlackBoard.getPath(this, null);
			waitingForPath = true;
			return;
		}
		tick_count += 2;
		if (this.atBusStop)
			return;
		
		Move.Action cur = this.path.getFirst().getAction();
		GridPoint pos = this.grid.getLocation(this);
		GridPoint newpos = this.grid.getLocation(this);
		
		switch (cur) {
			case NORTH:
				newpos = new GridPoint(pos.getX(), pos.getY() + 1);
				break;
			case SOUTH:
				newpos = new GridPoint(pos.getX(), pos.getY() - 1);
				break;
			case EAST:
				newpos = new GridPoint(pos.getX() + 1, pos.getY());
				break;
			case WEST:
				newpos = new GridPoint(pos.getX() - 1, pos.getY());
				break;
			default: //BUS
				Iterable<Element> objects = grid.getObjectsAt(pos.getX(), pos.getY());
				for (Element el: objects) {
					if (el instanceof BusStop)
					{
						((BusStop) el).addPedestrian(this);
						atBusStop = true;
						return;
					}
				}
				break;
		}
		
		if (newpos.getX() >= 0 && newpos.getX() < grid.getDimensions().getWidth()
				&& newpos.getY() >= 0 && newpos.getY() < grid.getDimensions().getHeight()) {
			Iterable<Element> objects = grid.getObjectsAt(newpos.getX(), newpos.getY());
			Road r = null;
			Car c = null;
			BusInstance bus = null;
			boolean isNextParking = false;
			for (Element el: objects) {
				if (el instanceof Road)
				{
					r = (Road) el;
					isNextParking = r.isParking();
				}
				if (el instanceof Car)
					c = (Car) el;
				if (c != null && r != null)
					break;
			}
			if (isNextParking) {
				this.path.pop();
				c.addPassenger(this);
				return;
			}
			else if (r != null) {
				Iterable<Element> objectsAtMyPlace = grid.getObjectsAt(pos.getX(), pos.getY());
				for (Element el: objectsAtMyPlace) {
					if (el instanceof Road)
					{
						grid.moveTo(this, newpos.getX(), newpos.getY());
						this.path.pop();
						return;
					}
				}

				if (bus == null && c == null && r.isCrossable() && (r.getTl() == null || r.getTl().getColor() == Color.RED)) {
					if (this.tl != null)
					{
						this.tl.unregister(this);
						this.tl = null;
					}
					grid.moveTo(this, newpos.getX(), newpos.getY());
					this.path.pop();
				}
				else if (r.isCrossable() && r.getTl().getColor() == Color.GREEN) {
					this.tl = r.getTl();
					this.tl.register(this);
				}
			}
			else {
				grid.moveTo(this, newpos.getX(), newpos.getY());
				this.path.pop();
			}
			this.setPosition(new Position(newpos.getX(), newpos.getY()));
		}
		else
		{
			Context<Element> context = ContextUtils.getContext(this);
			context.remove(this);
		}
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}
}