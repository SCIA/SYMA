package citySim;

import java.awt.Color;
import java.util.ArrayList;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import utils.*;

public class TrafficLight extends Tile {
	
	private ArrayList<Pedestrian> pedAwaiting;
	private ArrayList<Car> carAwaiting;
	private ArrayList<BusInstance> busAwaiting;
	
	public int tickCount = 0;
	
	private Strategy strat;
	
	public TrafficLight(Color color) {
		super(color);
		this.pedAwaiting = new ArrayList<>();
		this.carAwaiting = new ArrayList<>();
		this.busAwaiting = new ArrayList<>();
		this.strat = new Basic(this);
	}
	
	@ScheduledMethod(start = 1, interval = 1, priority = 1, shuffle=true)
	public void update() {
		String strategy = RunEnvironment.getInstance().getParameters().getString("Strategy");
		if (strategy.equals("pedestrianFirst")) {
			if (this.pedAwaiting.size() >= 3 && tickCount >= 3) {
				if (!(strat instanceof PedestrianFirst))
					strat = new PedestrianFirst(this);
				tickCount = 0;
			}
		}
		else if (strategy.equals("vehiculeFirst")) {
			if ((this.carAwaiting.size() + this.busAwaiting.size()) >= 3 && tickCount >= 3) {
				if (!(strat instanceof VehiculeFirst))
					strat = new VehiculeFirst(this);
				tickCount = 0;
			}
		}
		else if (strategy.equals("multi")) {
			if (!(this.strat instanceof Basic) && tickCount == 3) {
				strat = new Basic(this);
				tickCount = 0;
			}
			else if (this.strat instanceof Basic && this.pedAwaiting.size() >= 3 && tickCount >= 3) {
				strat = new PedestrianFirst(this);
				tickCount = 0;
			}
			else if (this.strat instanceof Basic && (this.carAwaiting.size() + this.busAwaiting.size()) >= 3 && tickCount >= 3) {
				strat = new VehiculeFirst(this);
				tickCount = 0;
			}
		}
		strat.execute();
		tickCount++;
		if (tickCount == 7)
			tickCount = 0;
	}
	
	public void register(Agent a) {
		if (a instanceof Pedestrian && !this.pedAwaiting.contains((Pedestrian) a))
			this.pedAwaiting.add((Pedestrian) a);
		else if (a instanceof Car && !this.carAwaiting.contains((Car) a))
			this.carAwaiting.add((Car) a);
		else if (a instanceof BusInstance && !this.busAwaiting.contains((BusInstance) a))
			this.busAwaiting.add((BusInstance) a);
	}

	public void unregister(Agent a) {
		if (a instanceof Pedestrian)
			this.pedAwaiting.remove((Pedestrian) a);
		else if (a instanceof Car)
			this.carAwaiting.remove((Car) a);
		else if (a instanceof BusInstance)
			this.busAwaiting.remove((BusInstance) a);
	}
	
	public ArrayList<Pedestrian> getAwaitPed() {
		return this.pedAwaiting;
	}
	
	public ArrayList<Car> getAwaitCars() {
		return this.carAwaiting;
	}
	
	public ArrayList<BusInstance> getAwaitBus() {
		return this.busAwaiting;
	}
}
