package citySim;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import java.util.TreeSet;

import utils.Move;
import utils.Position;
import utils.Status;

public class Dijkstra {
	public final int timeSideWay = 2;
	public final int timeRoad = 1;
	
	private class Node implements Comparable<Node> {
		private int dist = Integer.MAX_VALUE;
		private Node prev =  null;
		private Position position;
		private Status status;
		private Bus bus;
		
		public Node(Position position, Status status) {
			this.setStatus(status);
			this.position = position;
		}
		
		public Bus getBus() {
			return bus;
		}

		public void setBus(Bus bus) {
			this.bus = bus;
		}

		@Override
		public int compareTo(Node o) {
			if (dist > o.dist)
				return 1;
			else
				return -1;
		}
		
		public int getDist() {
			return dist;
		}

		public void setDist(int dist) {
			this.dist = dist;
		}

		public Node getPrev() {
			return prev;
		}

		public void setPrev(Node prev) {
			this.prev = prev;
		}
		
		public Position getPosition() {
			return position;
		}
		
		@Override
		public String toString() {
			return "pos" + position.getX() + " " + position.getY();
		}

		public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
		}
	}
	
	private Node[][] distPed;
	private Node[][] distRoad;
	private TreeSet<Node> nodes;
	private ArrayList<Position> blocked;
	private Car car = null;
	private Status status;
	
	private ArrayList<Element>[][] grid;
	
	
	public Dijkstra(ArrayList<Element>[][] grid, Status status, ArrayList<Position> blocked,  Car car) {
		this.status = status;
		this.grid = grid;
		distPed = new Node[grid.length][grid[0].length];
		distRoad = new Node[grid.length][grid[0].length];
		for (int i = 0; i < distPed.length; i++) {
			for (int j = 0; j < distPed[i].length; j++) {
				distPed[i][j] = new Node(new Position(i, j), Status.PED);
				distRoad[i][j] = new Node(new Position(i, j), status == Status.BUS ? Status.BUS : Status.CAR);
			}
		}
		nodes = new TreeSet<>();
		this.blocked = blocked;
		this.car = car;
	}
	
	public LinkedList<Move> getShortestPath(Position from, Position dest) {
		// Initialization
		Node elm = null;
		if (status == Status.PED)
			elm = distPed[grid[0].length - 1 - from.getY()][from.getX()];
		else
			elm = distRoad[grid[0].length - 1 - from.getY()][from.getX()];
		
		elm.setDist(0);
		while(elm != null && !(elm.getPosition().getY() == dest.getX() &&
				grid[0].length - 1 - elm.getPosition().getX() == dest.getY())) {
			updateNeighb(elm);
			elm = nodes.pollFirst();
		}
		if (elm == null) {
			System.err.println("not path from: " + from.getX() + " " + from.getY() + " to " + dest.getX() + " " + dest.getY());
			return null;
		}
		if (status == Status.PED || status == Status.CAR)
			return getPath(distPed[grid[0].length - 1 - dest.getY()][dest.getX()]);
		else
			return getPath(distRoad[grid[0].length - 1 - dest.getY()][dest.getX()]);
	}
	
	private void updateNeighb(Node node) {
		Road road = null;
		BusStop busStop = null;
		for (Object object: grid[node.getPosition().getY()][grid[0].length - 1 - node.getPosition().getX()]) {
			if (object instanceof Road) {
				road = (Road) object;
				break;
			} else if (object instanceof BusStop) {
				busStop = (BusStop) object;
				break;
			}
		}
		
		boolean pedestrian = node.getStatus() == Status.PED;
		int time = node.getStatus() == Status.PED ? timeSideWay : timeRoad;
		boolean parking = road != null ? road.isParking() : false;
		
		int x = node.getPosition().getX();
		int y = node.getPosition().getY();
		if (pedestrian || (road != null && road.getDirection().isSouth()) || (parking && isSide(x + 1, y)))
			updateCell(pedestrian, node, x + 1, y, time, parking, null);
		if (pedestrian || (road != null && road.getDirection().isNorth()) || (parking && isSide(x - 1, y)))
			updateCell(pedestrian, node, x - 1, y, time, parking, null);
		if (pedestrian || (road != null && road.getDirection().isEast()) || (parking && isSide(x, y + 1)))
			updateCell(pedestrian, node, x, y + 1, time, parking, null);
		if (pedestrian || (road != null && road.getDirection().isWest()) || (parking && isSide(x, y - 1)))
			updateCell(pedestrian, node, x, y - 1, time, parking, null);

		
		if (pedestrian && busStop != null) {
			for (Bus bus: busStop.getBuses()) {
				Iterator<Entry<BusStop, Integer>> iterator = bus.getBusStops().entrySet().iterator();
				while (iterator.hasNext()) {
					Entry<BusStop, Integer> prev = iterator.next();
					if (prev.getKey() == busStop && iterator.hasNext()) {
						Entry<BusStop, Integer> entry = iterator.next();
						updateCell(true, node, grid[0].length - 1 - entry.getKey().getPosition().getY(),
							entry.getKey().getPosition().getX(), prev.getValue(), false, bus);
					}
				}
			}
		}
	}
	
	private boolean isSide(int x, int y) {
		for (Object object: grid[y][grid[0].length - 1 - x]) {
			if (object instanceof Sidewalk)
				return true;
		}
		return false;
	}
	
	private void updateCell(boolean pedestrian, Node prev, int x, int y, int time, boolean parking, Bus bus) {
		if (x < 0 || y < 0 || x >= distRoad.length || y >= distRoad[0].length)
			return;
		Road road = null;
		Sidewalk sidewalk = null;
		for (Object object: grid[y][grid[0].length - 1 - x]) {
			if (object instanceof Road)
				road = (Road) object;
			else if (object instanceof Sidewalk)
				sidewalk = (Sidewalk) object;
		}
		boolean isPed = false;
		Position curPos = new Position(y, grid.length - 1 - x);
		if (blocked != null) {
			for (Position position : blocked) {
				if (position.equal(curPos))
					return;
			}
		}
		// building
		if (sidewalk == null && road == null)
			return;
		// transition from road to side
		if (sidewalk != null && !pedestrian && (!parking || (parking && prev.getStatus() == Status.BUS)))
			return;
		// ped or crossable road
		if ((road != null && road.isCrossable()) || road == null) {
			time = timeSideWay;
			isPed = sidewalk != null || pedestrian ? true : false;
		}
		// transition from side to road
		else if (pedestrian && (!road.isParking() || car == null || (car != null && !car.getPosition().equal(curPos)) || (car != null && car.hasPassenger()))) {
			return;
		}
		
		Node cur;
		if (isPed)
			cur = distPed[x][y];
		else
			cur = distRoad[x][y];
		if (bus != null && (prev.getBus() == null || !prev.getBus().equals(bus)))
			time += bus.getTimeToSpawn();
		if (cur.getDist() > prev.getDist() + time) {
			cur.setPrev(prev);
			if (bus != null)
				cur.setBus(bus);
			cur.setDist(prev.getDist() + time);
			nodes.add(cur);
		}
	}
	
	private LinkedList<Move> getPath(Node dest) {
		LinkedList<Move> actions = new LinkedList<>();
		while(dest.getPrev() != null) {
			if (dest.getPrev().getPosition().getX() - 1 == dest.getPosition().getX())
				actions.addFirst(new Move(Move.Action.NORTH, null));
			else if (dest.getPrev().getPosition().getX() + 1 == dest.getPosition().getX())
				actions.addFirst(new Move(Move.Action.SOUTH, null));
			else if (dest.getPrev().getPosition().getY() + 1 == dest.getPosition().getY())
				actions.addFirst(new Move(Move.Action.EAST, null));
			else if (dest.getPrev().getPosition().getY() - 1 == dest.getPosition().getY())
				actions.addFirst(new Move(Move.Action.WEST, null));
			else
				actions.addFirst(new Move(Move.Action.BUS, dest.getBus()));
			dest = dest.getPrev();
		}
		return actions;
	}
}