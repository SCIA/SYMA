package citySim;

import java.awt.Color;

import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class ElementStyle2D extends DefaultStyleOGL2D {

	@Override
	public Color getColor(Object o) {
		return ((Element)o).getColor();
	}
	
	@Override
	public VSpatial getVSpatial(Object o, VSpatial spatial) {
		if (spatial != null)
			return spatial;
		if (o instanceof Tile)
			return shapeFactory.createRectangle(15, 15);
		if (o instanceof Pedestrian)
			return shapeFactory.createCircle(4, 10);
		if (o instanceof Car)
			return shapeFactory.createCircle(7, 10);
		if (o instanceof BusInstance)
			return shapeFactory.createCircle(7, 10);
		return spatial;
	}
	
}
