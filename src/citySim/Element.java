package citySim;

import java.awt.Color;

public abstract class Element {
	protected Color color;

	public Element(Color color2) {
		this.color = color2;
	}
	
	public Color getColor() {
		return this.color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
}
