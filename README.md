Simulation
==========

Concevoir et réaliser un système multi-agent permettant de représenter les mouvements des véhicules et
des voyageurs dans un quartier. Le quartier est décrit à minima par des routes, des feux de signalisation et
des arrêts de bus. Les voyageurs ont une origine et une destination dans le quartier qu’ils peuvent atteindre
en voiture, à pieds ou en transport en commun. L’objectif est de simuler diverses stratégies pour les plans de
feux de signalisation : durée de feu vert fixée, durée dynamique dépendant de la longueur des files d’attente,
de la présence de bus dans la file d’attente, etc. Les stratégies seront comparées selon le temps de parcours des
voyageurs et des véhicules.
Pour ce faire, il vous faudra :
1. Représenter le quartier
2. Représenter les modes de transport
3. Définir les mouvements des usagers et des véhicules

Tout enrichissement du projet sera gratifié de bonus.